﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO.Ports;
using System.Text;
using Timer = System.Timers.Timer;
using System.Timers;
using NLog;

namespace SerialPortLib
{
    public class SerialPortReceiver
    {
        private readonly Timer _rxTimeoutTimer;
        private readonly SerialPort _port;
        private readonly StringBuilder _rxText;
        private readonly List<byte> _rxBinary = new List<byte>(8192);
        private bool _isStaffing;
        private Thread _rxPortLinuxThread;
        private readonly Logger _nlog;

        #region Свойства
        /// <summary>
        /// Протокол, на работу с которым настроено устройство
        /// </summary>
        public DelimiterMode DelimiterMode { get; set; }

        /// <summary>
        /// Количество принятных байт данных
        /// </summary>
        public long RxByteCount { get; set; }

        /// <summary>
        /// Количество принятых текстовых строк (разделённых переводом строки)
        /// Используется только для текстовых протоколов
        /// </summary>
        public long RxStringLineCount { get; set; }

        /// <summary>
        /// Количество принятых пакетов WRB-b
        /// </summary>
        public long RxWrbbPacketCount { get; set; }
        #endregion

        #region Делегаты и События
        public event ReceiveTextEventHandler ReceiveText;
        public delegate void ReceiveTextEventHandler(string rxText);

        public event ReceiveBinaryEventHandler ReceiveBinary;
        public delegate void ReceiveBinaryEventHandler(List<byte> rxList);
        #endregion

        #region Конструкторы
        public SerialPortReceiver(SerialPort port, DelimiterMode delimiterMode)
        {
            _port = port;
            _port.DataReceived += PortOnDataReceived;

            _rxText = new StringBuilder();

            _rxTimeoutTimer = new Timer { Interval = 500 };
            _rxTimeoutTimer.Elapsed += RxTimeoutTimerOnElapsed;

            DelimiterMode = delimiterMode;

            _nlog = LogManager.GetCurrentClassLogger();
        }
        #endregion

        public void StartRx()
        {
            if (Utils.IsLinux)
            {
                _rxPortLinuxThread = new Thread(RxLinuxPortProcess);
                _rxPortLinuxThread.Start(_port);
            }
        }

        public void StopRx()
        {
            if (Utils.IsLinux)
            {
                _rxPortLinuxThread.Abort();
            }
        }

        private void RxLinuxPortProcess(object o)
        {
            if (!(o is SerialPort port))
            {
                return;
            }

            while (true)
            {
                if (port.IsOpen)
                {
                    int rxCount = port.BytesToRead;

                    if (rxCount > 0)
                    {
                        var buf = new byte[rxCount];
                        port.Read(buf, 0, rxCount);
                        RxPortProcess(buf);
                    }

                    Thread.Sleep(1);
                }
            }
        }

        private void RxPortProcess(byte[] buf)
        {
            if (DelimiterMode == DelimiterMode.Text)
            {
                foreach (byte data in buf)
                {
                    if ((data == '\r') || (data == '\n'))
                    {
                        if (_rxText.Length > 0)
                        {
                            RxStringLineCount++;
                            ReceiveText?.Invoke(_rxText.ToString());
                            _rxText.Clear();
                        }
                    }
                    else
                    {
                        _rxText.Append((char) data);
                    }
                }

                _rxTimeoutTimer.Enabled = _rxText.Length > 0;
            }
            else if (DelimiterMode == DelimiterMode.Arb)
            {
                foreach (byte data in buf)
                {
                    if (data == 0x7E)
                    {
                        _isStaffing = false;
                        if (_rxBinary.Count > 0)
                        {
                            RxWrbbPacketCount++;
                            ReceiveBinary?.Invoke(_rxBinary);
                            _rxBinary.Clear();
                        }
                    }
                    else if (data == 0x7D)
                    {
                        _isStaffing = true;
                    }
                    else
                    {
                        if (_isStaffing)
                        {
                            _rxBinary.Add((byte)(data ^ 0x20));
                            _isStaffing = false;
                        }
                        else
                        {
                            _rxBinary.Add(data);
                        }
                    }
                }

                _rxTimeoutTimer.Enabled = _rxBinary.Count > 0;
            }
        }

        #region Обработка событий
        void RxTimeoutTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            _nlog.Error($"Таймаут приёма данных. _rxText = {_rxText}");
            _rxText.Clear();
            _rxTimeoutTimer.Enabled = false;
        }

        private void PortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            _rxTimeoutTimer.Enabled = false;

            var port = (SerialPort)sender;
            byte[] buf;
            try
            {
                buf = new byte[port.BytesToRead];
                port.Read(buf, 0, buf.Length);
            }
            catch
            {
                return;
            }

            RxByteCount += buf.Length;

            RxPortProcess(buf);
        }
        #endregion
    }

    public enum DelimiterMode
    {
        Text,
        Arb,
    }
}
