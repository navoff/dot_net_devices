﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text.RegularExpressions;

namespace SerialPortLib
{
    public class Utils
    {
        public static bool IsLinux { get; }

        static Utils()
        {
            IsLinux = GetIsLinux();
        }

        public static bool GetIsLinux()
        {
            int p = (int)Environment.OSVersion.Platform;
            return (p == 4) || (p == 6) || (p == 128);
        }

        public static string GetPortNameFromInfo(string info)
        {
            string res = "";

            if (IsLinux)
            {
                res = info;
            }
            else
            {
                var match = Regex.Match(info, @"^.*(COM\d+).*");

                if (match.Success)
                {
                    if (match.Groups.Count >= 2)
                    {
                        res = match.Groups[1].Value;
                    }
                }
            }

            return res;
        }

        public static List<string> GetPortInfoes()
        {
            List<string> res;

            if (IsLinux)
            {
                res = GetPortNames();
            }
            else
            {
                res = new List<string>();
                var mSearch = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0");
                var mReturn = mSearch.Get();

                foreach (var o in mReturn)
                {
                    var mObj = (ManagementObject)o;
                    if (mObj["Name"] != null)
                    {
                        var name = mObj["Name"]?.ToString();
                        if (name.Contains("(COM"))
                        {
                            res.Add(name);
                        }
                    }
                }
            }

            return res;
        }

        public static List<string> GetPortNames()
        {
            List<string> names = SerialPort.GetPortNames().ToList();

            for (int i = 0; i < names.Count; i++)
            {
                string name = Regex.Replace(names[i], @"[^\u0000-\u007F]", string.Empty);
                names[i] = name;
            }

            // отсортировать в обратном порядке, чтобы быстрее искалось
            // т.к. подключенные устройства скорее всего окажутся в конце списка
            names.Sort((a, b) => -1 * string.Compare(a, b, StringComparison.Ordinal));

            return names;
        }

        public static string GetOsVersionName()
        {
            if (IsLinux)
            {
                return GetLinuxOsVersionName();
            }
            else
            {
                return GetWinOsVersionName();
            }
        }

        public static string GetLinuxOsVersionName()
        {
            return $"Linux ({Environment.OSVersion})";
        }

        public static string GetWinOsVersionName()
        {
            Version version = Environment.OSVersion.Version;

            string osVersionName = "не известна";

            if ((version.Major == 10) && (version.Minor == 0))
            {
                osVersionName = "Windows 10";
            }
            else if ((version.Major == 6) && (version.Minor == 3))
            {
                osVersionName = "Windows 8.1";
            }
            else if ((version.Major == 6) && (version.Minor == 2))
            {
                osVersionName = "Windows 8";
            }
            else if ((version.Major == 6) && (version.Minor == 1))
            {
                osVersionName = "Windows 7";
            }
            else if ((version.Major == 6) && (version.Minor == 0))
            {
                osVersionName = "Windows Vista";
            }
            else if ((version.Major == 5) && (version.Minor == 2))
            {
                osVersionName = "Windows 2003";
            }
            else if ((version.Major == 5) && (version.Minor == 1))
            {
                osVersionName = "Windows XP";
            }

            return osVersionName;
        }
    }
}
