﻿namespace SerialPortLib
{
    partial class ControlSerialPort
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSerialPort = new System.Windows.Forms.GroupBox();
            this.lBaudrate = new System.Windows.Forms.Label();
            this.BtnOpenCloseSerialPort = new System.Windows.Forms.Button();
            this.cmbSerialPort = new System.Windows.Forms.ComboBox();
            this.tbBaudrate = new System.Windows.Forms.TextBox();
            this.gbSerialPort.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSerialPort
            // 
            this.gbSerialPort.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSerialPort.Controls.Add(this.lBaudrate);
            this.gbSerialPort.Controls.Add(this.BtnOpenCloseSerialPort);
            this.gbSerialPort.Controls.Add(this.cmbSerialPort);
            this.gbSerialPort.Controls.Add(this.tbBaudrate);
            this.gbSerialPort.Location = new System.Drawing.Point(0, 0);
            this.gbSerialPort.Margin = new System.Windows.Forms.Padding(0);
            this.gbSerialPort.Name = "gbSerialPort";
            this.gbSerialPort.Size = new System.Drawing.Size(633, 40);
            this.gbSerialPort.TabIndex = 18;
            this.gbSerialPort.TabStop = false;
            this.gbSerialPort.Text = "COM-порт";
            // 
            // lBaudrate
            // 
            this.lBaudrate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lBaudrate.Location = new System.Drawing.Point(351, 16);
            this.lBaudrate.Name = "lBaudrate";
            this.lBaudrate.Size = new System.Drawing.Size(107, 13);
            this.lBaudrate.TabIndex = 8;
            this.lBaudrate.Text = "Скорость (бит/с):";
            this.lBaudrate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnOpenCloseSerialPort
            // 
            this.BtnOpenCloseSerialPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOpenCloseSerialPort.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BtnOpenCloseSerialPort.Location = new System.Drawing.Point(553, 11);
            this.BtnOpenCloseSerialPort.Name = "BtnOpenCloseSerialPort";
            this.BtnOpenCloseSerialPort.Size = new System.Drawing.Size(74, 23);
            this.BtnOpenCloseSerialPort.TabIndex = 5;
            this.BtnOpenCloseSerialPort.Text = "Открыть";
            this.BtnOpenCloseSerialPort.UseVisualStyleBackColor = true;
            // 
            // cmbSerialPort
            // 
            this.cmbSerialPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSerialPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSerialPort.FormattingEnabled = true;
            this.cmbSerialPort.Location = new System.Drawing.Point(5, 13);
            this.cmbSerialPort.Name = "cmbSerialPort";
            this.cmbSerialPort.Size = new System.Drawing.Size(340, 21);
            this.cmbSerialPort.TabIndex = 6;
            this.cmbSerialPort.DropDown += new System.EventHandler(this.cmbSerialPort_DropDown);
            // 
            // tbBaudrate
            // 
            this.tbBaudrate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBaudrate.Location = new System.Drawing.Point(464, 13);
            this.tbBaudrate.Name = "tbBaudrate";
            this.tbBaudrate.Size = new System.Drawing.Size(86, 20);
            this.tbBaudrate.TabIndex = 9;
            // 
            // ControlSerialPort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbSerialPort);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "ControlSerialPort";
            this.Size = new System.Drawing.Size(633, 41);
            this.gbSerialPort.ResumeLayout(false);
            this.gbSerialPort.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSerialPort;
        private System.Windows.Forms.Label lBaudrate;
        private System.Windows.Forms.ComboBox cmbSerialPort;
        private System.Windows.Forms.TextBox tbBaudrate;
        public System.Windows.Forms.Button BtnOpenCloseSerialPort;
    }
}
