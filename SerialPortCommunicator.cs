﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using NLog;

namespace SerialPortLib
{
    public abstract class SerialPortCommunicator
    {
        private readonly Logger _nlog;
        protected readonly SerialPort Port;
        private readonly SerialPortReceiver _serialPortReceiver;

        #region Свойства
        public bool IsConnected { get; protected set; }
        #endregion

        #region Делегаты и События
        public event ReceiveTextEventHandler ReceiveText;
        public delegate void ReceiveTextEventHandler(string rxText);

        protected event ReceiveBinaryEventHandler ReceiveBinary;
        protected delegate void ReceiveBinaryEventHandler(List<byte> rxList);

        public event DisconnectedEventHandler Disconnected;
        public delegate void DisconnectedEventHandler();
        #endregion

        protected SerialPortCommunicator(DelimiterMode delimiterMode)
        {
            Port = new SerialPort();

            _nlog = LogManager.GetLogger("SerialPortTraffic");

            _serialPortReceiver = new SerialPortReceiver(Port, delimiterMode);
            _serialPortReceiver.ReceiveText += SerialPortReceiverOnReceiveText;
            _serialPortReceiver.ReceiveBinary += SerialPortReceiverOnReceiveBinary;
        }

        protected virtual void SerialPortReceiverOnReceiveBinary(List<byte> rxlist)
        {
            if (_nlog.IsTraceEnabled)
            {
                var traceMsg = new StringBuilder("Rx: ");
                foreach (var b in rxlist)
                {
                    traceMsg.Append("0x");
                    traceMsg.Append(b.ToString("X2"));
                    traceMsg.Append(" ");
                }

                _nlog.Trace(traceMsg.ToString());
            }

            ReceiveBinary?.Invoke(rxlist);
        }

        protected virtual void SerialPortReceiverOnReceiveText(string rxtext)
        {
            if (_nlog.IsTraceEnabled)
            {
                var traceMsg = new StringBuilder("Rx: ");
                foreach (var b in rxtext)
                {
                    traceMsg.Append("0x");
                    traceMsg.Append(((byte)b).ToString("X2"));
                    traceMsg.Append(" ");
                }

                _nlog.Trace(traceMsg.ToString());
            }

            ReceiveText?.Invoke(rxtext);
        }

        public void Connect(string name, int baudRate, Handshake handshake = Handshake.None)
        {
            Connect(name, baudRate, _serialPortReceiver.DelimiterMode, handshake);
        }

        public void Connect(
            string name, int baudRate,
            DelimiterMode delimiter,
            Handshake handshake = Handshake.None)
        {
            Port.BaudRate = baudRate;
            Port.PortName = name;
            Port.Parity = Parity.None;
            Port.DataBits = 8;
            Port.StopBits = StopBits.One;
            Port.Handshake = handshake;

            _serialPortReceiver.DelimiterMode = delimiter;

            var res = SerialPortConnector.TryOpenPortAsync(Port, 10000);
            if (res == SerialPortConnector.Result.Opened)
            {
                Port.DiscardInBuffer(); // Очищаем приемный буфер
                _serialPortReceiver.StartRx();
                IsConnected = true;
            }
        }

        public void Disconnect()
        {
            _serialPortReceiver.StopRx();
            SerialPortConnector.TryClosePortAsync(Port, 2000);
            IsConnected = false;
            Disconnected?.Invoke();
        }

        /// <summary>
        /// Очистить буффер на отправку
        /// </summary>
        public void FlushTx()
        {
            Port.DiscardOutBuffer(); // xxx Пока отправка не вынесена в отдельный поток будет так
        }

        public void Send(string text)
        {
            if (IsConnected == false) return;

            if (_nlog.IsTraceEnabled)
            {
                var traceMsg = new StringBuilder("Tx: ");
                foreach (var b in text)
                {
                    traceMsg.Append("0x");
                    traceMsg.Append(((byte)b).ToString("X2"));
                    traceMsg.Append(" ");
                }

                _nlog.Trace(traceMsg.ToString());
            }

            Port.Write(text);
        }

        public void Send(byte[] array)
        {
            if (IsConnected == false) return;

            if (_nlog.IsTraceEnabled)
            {
                var traceMsg = new StringBuilder("Tx: ");
                foreach (var b in array)
                {
                    traceMsg.Append("0x");
                    traceMsg.Append(b.ToString("X2"));
                    traceMsg.Append(" ");
                }

                _nlog.Trace(traceMsg.ToString());
            }

            try
            {
                Port.Write(array, 0, array.Length);
            }
            catch (Exception e)
            {
                _nlog.Error("Ошибка записи в порт: " + e.Message);
            }
        }
    }
}
