﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace SerialPortLib
{
    public partial class ControlSerialPort : UserControl
    {
        public int Baudrate
        {
            get
            {
                if (int.TryParse(tbBaudrate.Text, out int baudrate))
                {
                    return baudrate;
                }
                else
                {
                    return 9600;
                }
            }
            set => tbBaudrate.Text = value.ToString();
        }

        public string SerialPortName
        {
            get => cmbSerialPort.Text;
            set
            {
                cmbSerialPort.Items.Clear();

                int idx = -1;
                var portnames = Utils.GetPortInfoes();
                for (int i = 0; i < portnames.Count; i++)
                {
                    cmbSerialPort.Items.Add(portnames[i]);
                    if (string.IsNullOrWhiteSpace(value) == false)
                    {
                        if (portnames[i].Equals(value, StringComparison.Ordinal))
                        {
                            idx = i;
                        }
                    }
                }
                cmbSerialPort.SelectedIndex = idx;
            }
        } 

        public ControlSerialPort()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Изменить состояние кнопок подключения к COM-порту
        /// </summary>
        /// <param name="enable"></param>
        public void SetGuiState(bool enable)
        {
            BtnOpenCloseSerialPort.Enabled = enable;
            tbBaudrate.Enabled = enable;
            cmbSerialPort.Enabled = enable;
        }

        /// <summary>
        /// Заполнить список доступных портов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbSerialPort_DropDown(object sender, EventArgs e)
        {
            var cmb = (ComboBox)sender;
            var portnames = Utils.GetPortInfoes();
            cmb.Items.Clear();
            foreach (string portname in portnames)
            {
                cmb.Items.Add(portname);
            }
        }

        public void UpdateLanguage()
        {
        }
    }
}
