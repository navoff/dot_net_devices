﻿using NLog;
using System;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace SerialPortLib
{
    internal class SerialPortConnectorParam
    {
        internal SerialPortConnector.Result Result;

        internal SerialPort Port;

        internal SerialPortConnectorParam(SerialPort port)
        {
            Port = port;
            Result = SerialPortConnector.Result.Undef;
        }
    }

    /// <summary>
    /// Класс с различными способами открытия/закрытия последовательного порта
    /// </summary>
    internal static class SerialPortConnector
    {
        private static Logger Nlog => LogManager.GetCurrentClassLogger();

        private static void OpenPort(object o)
        {
            if (o is SerialPortConnectorParam param)
            {
                param.Result = TryOpenPort(param.Port);
            }
        }

        public static Result TryOpenPort(SerialPort port)
        {
            Nlog.Trace($"TryOpenPort: {port.PortName}, {port.BaudRate}");

            Result res = Result.Closed;

            try
            {
                port.Open();
                Nlog.Trace("TryOpenPort. Порт открыт");
            }
            catch (UnauthorizedAccessException ex)
            {
                res = Result.AlreadyOpen;
                Nlog.Error("TryOpenPort. Ошибка: " + ex.Message);
            }
            catch (IOException ex)
            {
                res = Result.IOException;
                Nlog.Error("TryOpenPort. Ошибка: " + ex.Message);
            }
            catch (Exception ex)
            {
                res = Result.Exception;
                Nlog.Error("TryOpenPort. Ошибка: " + ex.Message);
            }

            if (port.IsOpen)
            {
                return Result.Opened;
            }

            return res;
        }

        public static Result TryOpenPortAsync(SerialPort port, int delay)
        {
            Nlog.Trace("TryOpenPortAsync");

            SerialPortConnectorParam connectorParam = new SerialPortConnectorParam(port);

            Thread t = new Thread(OpenPort);

            t.Start(connectorParam);

            for (int i = 0; i < delay; i++)
            {
                Thread.Sleep(1);
                if (t.IsAlive == false)
                {
                    break;
                }
            }

            try
            {
                if (t.IsAlive)
                {
                    t.Abort();
                }
            }
            catch (ThreadAbortException)
            {
                connectorParam.Result = Result.ThreadAbortError;
            }

            return connectorParam.Result;
        }

        private static void ClosePort(Object o)
        {
            if (o is SerialPortConnectorParam param)
            {
                param.Result = TryClosePort(param.Port);
            }
            else
            {
                Nlog.Error("ClosePort - Пустой параметр при вызове функции");
            }
        }

        private static Result TryClosePort(SerialPort port)
        {
            if (port == null) return Result.NullParam;

            try
            {
                port.Close();
            }
            catch (IOException ioEx)
            {
                Nlog.Error(
                    ioEx, "TryClosePort - Исключение при закрытии порта: \r\n{0}", ioEx);
                return Result.IOException;
            }
            catch (Exception ex)
            {
                Nlog.Error(ex, "TryClosePort - Исключение при закрытии порта: \r\n{0}", ex);
                return Result.Exception;
            }

            return Result.Closed;
        }

        public static Result TryClosePortAsync(SerialPort port, int delay)
        {
            Nlog.Trace("TryClosePortAsync");

            SerialPortConnectorParam connectorParam = new SerialPortConnectorParam(port);

            Thread t = new Thread(ClosePort);

            t.Start(connectorParam);

            bool timeout = true;

            for (int i = 0; i < delay; i++)
            {
                Thread.Sleep(1);
                if (t.IsAlive == false)
                {
                    timeout = false;
                    break;
                }
            }

            if (timeout)
            {
                Nlog.Error("Таймут при выполннеии процедуры закрытия порта");
            }

            try
            {
                if (t.IsAlive)
                {
                    //t.Abort();
                    connectorParam.Result = Result.Timeout;
                }
            }
            catch (Exception ex)
            {
                connectorParam.Result = Result.ThreadAbortError;
                Nlog.Error(ex, "TryClosePortAsync - Ошибка в работе потока закрытия порта: \r\n{0}", ex);
            }

            return connectorParam.Result;
        }

        /// <summary>
        /// Возможный результат открытия/закрытия порта
        /// </summary>
        public enum Result
        {
            Opened,
            AlreadyOpen,
            Disconnect,
            Closed,
            NullParam,
            ThreadAbortError,
            IOException,
            Exception,
            Timeout,
            Undef,
        }
    }
}
